#VB.NET Convert Unicode to Arabic-1001#

### The problem ###

Arabic supporting POS printers usually require the text to be encoded as charset/codepage-1001, windows doesn't seem to support it and there's no automated way to do the conversion.

### Characters ###

when printed the arabic characters will look like that

![ara-1001.png](https://bitbucket.org/repo/ak8xz8/images/799770251-ara-1001.png)

### The repository ###

this repository hosts a VB.net project that can convert most of the characters.
