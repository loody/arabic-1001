﻿
Public Class Codepage1001

    Shared connectedLetters As String = "یظئطكمنتلبيسشجحخهعغفقثصض"
    Shared ret As String = ""
    Shared enbuf As String = ""
    Shared arbuf As String = ""
    Shared en As Boolean = False
    Shared preven As Boolean = False

    Shared Function convert(ByVal unicodestring As String)
        ret = ""
        en = False
        enbuf = ""
        arbuf = ""
        Dim chars128 As String = "ﺀء"
        Dim chars129 As String = "ﺁﺂآ"
        Dim chars130 As String = "ﺃﺄأ"
        Dim chars131 As String = "ﺅﺆؤ"
        Dim chars132 As String = "ﺇﺈإ"
        Dim chars133 As String = "ﺉﺊﺋﺌئ"
        Dim chars134 As String = "ﺍﺎاا"
        Dim chars135 As String = "بﺏﺐ"
        Dim chars137 As String = "ﺓﺔة"
        Dim chars138 As String = "تﺕﺖ"
        Dim chars140 As String = "ثﺙﺚ"
        Dim chars142 As String = "ﺩﺪد"
        Dim chars143 As String = "ﺫﺬذ"
        Dim chars144 As String = "ﺭﺮر"
        Dim chars145 As String = "ﺯﺰز"
        Dim chars146 As String = "سﺱﺲ"
        Dim chars149 As String = "ﺵﺶش"
        Dim chars152 As String = "ﺹﺺص"
        Dim chars155 As String = "ﺽﺾض"
        Dim chars158 As String = "ﻁﻂﻃﻄط"
        Dim chars159 As String = "ﻅﻆﻇﻈظ"
        Dim chars160 As String = "عﻉﻊع"
        Dim chars162 As String = "غﻍﻎ"
        Dim chars164 As String = "ﻑﻒف"
        Dim chars166 As String = "قﻕﻖ"
        Dim chars168 As String = "ﻙﻚك"
        Dim chars170 As String = "لﻝﻞ"
        Dim chars171 As String = "ﻵﻶﻷﻸ"
        Dim chars172 As String = "ﻹﻺ"
        Dim chars173 As String = "ﻻﻼ"
        Dim chars175 As String = "ﻡﻢم"
        Dim chars177 As String = "نﻥﻦ"
        Dim chars179 As String = "ﻩﻪه"
        Dim chars181 As String = "ﻭﻮو"
        Dim chars182 As String = "ﻯﻰﻱﻲيیى"
        Dim chars193 As String = "ﺝﺞج"
        Dim chars195 As String = "ﺡﺢح"
        Dim chars197 As String = "ﺥﺦخ"
        Dim prev As String = ""
        For i = 0 To unicodestring.Length - 1
            If chars128.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("128")
            ElseIf chars129.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isConneted(prev) Then
                    en = False
                    append("189")
                Else
                    en = False
                    append("129")
                End If
            ElseIf chars130.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isConneted(prev) Then
                    en = False
                    append("190")
                Else
                    en = False
                    append("130")
                End If
            ElseIf chars131.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isConneted(prev) Then
                    en = False
                    append("131")
                Else
                    en = False
                    append("131")
                End If
            ElseIf chars132.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isConneted(prev) Then
                    en = False
                    append("132")
                Else
                    en = False
                    append("132")
                End If
            ElseIf chars133.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    If isConneted(prev) Then
                        en = False
                        append("191")
                    Else
                        en = False
                        append("183")
                    End If
                Else
                    en = False
                    append("133")
                End If
            ElseIf chars134.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isConneted(prev) Then
                    en = False
                    append("192")
                Else
                    en = False
                    append("134")
                End If
            ElseIf chars135.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("135")
                Else
                    en = False
                    append("136")
                End If
            ElseIf chars137.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isConneted(prev) Then
                    en = False
                    append("137")
                Else
                    en = False
                    append("137")
                End If
            ElseIf chars138.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("138")
                Else
                    en = False
                    append("139")
                End If
            ElseIf chars140.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("140")
                Else
                    en = False
                    append("141")
                End If
            ElseIf chars142.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("142")
            ElseIf chars143.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("143")
            ElseIf chars144.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("144")
            ElseIf chars145.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("145")
            ElseIf chars146.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("146")
                Else
                    en = False
                    append("148")
                End If
            ElseIf chars149.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("149")
                Else
                    en = False
                    append("151")
                End If
            ElseIf chars152.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("152")
                Else
                    en = False
                    append("154")
                End If
            ElseIf chars155.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("155")
                Else
                    en = False
                    append("157")
                End If
            ElseIf chars158.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("158")
            ElseIf chars159.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("159")
            ElseIf chars160.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("160")
                Else
                    en = False
                    append("161")
                End If
            ElseIf chars162.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("162")
                Else
                    en = False
                    append("163")
                End If
            ElseIf chars164.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("164")
                Else
                    en = False
                    append("165")
                End If
            ElseIf chars166.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("166")
                Else
                    en = False
                    append("167")
                End If
            ElseIf chars168.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("168")
                Else
                    en = False
                    append("169")
                End If
            ElseIf chars170.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("170")
                Else
                    If isLamAlef(i + 1, unicodestring.Length, unicodestring) Then
                        i += 1
                        en = False
                        append(getLamAlefCode(i + 1, unicodestring.Length, unicodestring))
                    Else
                        en = False
                        append("174")
                    End If
                End If
            ElseIf chars171.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("171")
            ElseIf chars172.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("172")
            ElseIf chars173.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("173")
            ElseIf chars175.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("175")
                Else
                    en = False
                    append("176")
                End If
            ElseIf chars177.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("177")
                Else
                    en = False
                    append("178")
                End If
            ElseIf chars179.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("179")
                Else
                    en = False
                    append("180")
                End If
            ElseIf chars181.Contains(Mid(unicodestring, i + 1, 1)) Then
                en = False
                append("181")
            ElseIf chars182.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    If isConneted(prev) Then
                        en = False
                        append("209")
                    Else
                        en = False
                        append("182")
                    End If
                Else
                    en = False
                    append("184")
                End If
            ElseIf chars193.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("193")
                Else
                    en = False
                    append("194")
                End If
            ElseIf chars195.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("195")
                Else
                    en = False
                    append("196")
                End If
            ElseIf chars197.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    en = False
                    append("197")
                Else
                    en = False
                    append("198")
                End If
            Else
                Dim x = AscW(Mid(unicodestring, i + 1, 1)).ToString.PadLeft(3, "0")
                If x.Substring(x.Length - 3, 3).Equals("032") Then
                    append(x.Substring(x.Length - 3, 3))
                Else
                    If Integer.Parse(x.Substring(x.Length - 3, 3)) > 255 Then
                        Dim y = AscW("?").ToString.PadLeft(3, "0")
                        append(y.Substring(y.Length - 3, 3))
                    Else
                        en = True
                        append(x.Substring(x.Length - 3, 3))
                    End If
                End If
            End If
            prev = Mid(unicodestring, i + 1, 1)
        Next
        ret += enbuf + arbuf
        enbuf = ""
        Return ret
    End Function

    Private Shared Function isCut(ByVal pos, ByVal len, ByVal full)
        If pos = len Then
            Return True
        End If
        If Mid(full.ToString, pos + 1, 1).Equals(" ") Then
            Return True
        End If
        Return False
    End Function

    Private Shared Sub append(ByVal chr As String)
        If en = preven Then
            'same buffer
            If en Then
                enbuf += chr
            Else
                arbuf = chr + arbuf
            End If
        Else
            'another buffer
            preven = en
            If en Then
                'new en old ar
                'handle spaces
                If arbuf.Length > 3 Then
                    Dim pass = False
                    For i = 1 To arbuf.Length - 1
                        If Not Mid(arbuf, i, 3).Equals("032") Then
                            pass = True
                        End If
                        i += 2
                    Next
                    If pass Then
                        While (Left(arbuf, 3).Equals("032"))
                            arbuf = arbuf.Substring(3, arbuf.Length - 3) + "032"
                        End While
                    End If
                End If
                ret += arbuf
                arbuf = ""
                enbuf += chr
            Else
                ret += enbuf
                enbuf = ""
                arbuf = chr + arbuf
            End If
        End If
    End Sub

    Private Shared Function isLamAlef(ByVal pos, ByVal len, ByVal full)
        If pos = len Then
            Return False
        End If
        If "أإا".Contains(Mid(full.ToString, pos + 1, 1)) Then
            Return True
        End If
        Return False
    End Function

    Private Shared Function getLamAlefCode(ByVal pos, ByVal len, ByVal full)
        If Mid(full.ToString, pos + 1, 1).Equals("ا") Then
            Return "173"
        End If
        If Mid(full.ToString, pos + 1, 1).Equals("إ") Then
            Return "172"
        End If
        If Mid(full.ToString, pos + 1, 1).Equals("أ") Then
            Return "171"
        End If
        Return ""
    End Function


    Private Shared Function isConneted(ByVal ch As String)
        If connectedLetters.Contains(ch) And Not ch.Equals("") Then
            Return True
        End If
        Return False
    End Function


End Class
