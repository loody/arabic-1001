﻿Imports Microsoft.PointOfService
Imports System.Text

Public Class Class1
    Shared connectedLetters As String = "ظئطكمنتلبيسشجحخهعغفقثصض"

    Public Shared Sub print()
        Dim ar = "كيبوردك للكتابة بالعربي"
        MsgBox(convert(ar))
        '        Return
        Dim x = New OposPOSPrinter_CCO.OPOSPOSPrinter
        x.Open("Star TSP100 Cutter (TSP143)_1")
        x.ClaimDevice(1000)
        x.DeviceEnabled = True
        x.CharacterSet = 1001
        Dim t As PrinterStation = PrinterStation.Receipt
        x.BinaryConversion = 2
        x.PrintNormal(t, convert(ar))

        x.CutPaper(100)
        x.DeviceEnabled = False
        x.ReleaseDevice()
        x.Close()

    End Sub

    Private Shared Function convert(ByVal unicodestring As String)
        '        Dim codes As String = "128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 205 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 214 242 243 244 245 246 247 248 249 250 251 252 253 254 255 256"
        '       Dim chars As String = "ﺀ ﺁ ﺃ ﺅ ﺇ ﺉ "
        Dim chars128 As String = "ﺀء"
        Dim chars129 As String = "ﺁﺂ"
        Dim chars130 As String = "ﺃﺄأ"
        Dim chars131 As String = "ﺅﺆؤ"
        Dim chars132 As String = "ﺇﺈإ"
        Dim chars133 As String = "ﺉﺊﺋﺌئ"
        Dim chars134 As String = "ﺍﺎاا"
        Dim chars135 As String = "بﺏﺐ"
        Dim chars137 As String = "ﺓﺔة"
        Dim chars138 As String = "تﺕﺖ"
        Dim chars140 As String = "ﺙﺚ"
        Dim chars141 As String = "ﺛﺜ"
        Dim chars142 As String = "ﺩﺪد"
        Dim chars143 As String = "ﺫﺬذ"
        Dim chars144 As String = "ﺭﺮر"
        Dim chars145 As String = "ﺯﺰز"
        Dim chars146 As String = "ﺱﺲ"
        Dim chars148 As String = "ﺳﺴ"
        Dim chars149 As String = "ﺵﺶ"
        Dim chars151 As String = "ﺷﺸ"
        Dim chars152 As String = "ﺹﺺ"
        Dim chars154 As String = "ﺻﺼ"
        Dim chars155 As String = "ﺽﺾ"
        Dim chars157 As String = "ﺿﻀ"
        Dim chars158 As String = "ﻁﻂﻃﻄط"
        Dim chars159 As String = "ﻅﻆﻇﻈظ"
        Dim chars160 As String = "عﻉﻊ"
        Dim chars161 As String = "ﻋﻌ"
        Dim chars162 As String = "ﻍﻎ"
        Dim chars163 As String = "ﻏﻐ"
        Dim chars164 As String = "ﻑﻒ"
        Dim chars165 As String = "ﻓﻔ"
        Dim chars166 As String = "ﻕﻖ"
        Dim chars167 As String = "ﻗﻘ"
        Dim chars168 As String = "ﻙﻚك"
        '        Dim chars169 As String = "ك ﻛ ﻜ"
        Dim chars170 As String = "لﻝﻞ"
        Dim chars171 As String = "ﻵﻶﻷﻸ"
        Dim chars172 As String = "ﻹﻺ"
        Dim chars173 As String = "ﻻﻼ"
        '        Dim chars174 As String = "ﻟﻠ"
        Dim chars175 As String = "ﻡﻢ"
        Dim chars176 As String = "ﻣﻤ"
        Dim chars177 As String = "ﻥﻦ"
        Dim chars178 As String = "ﻧﻨ"
        Dim chars179 As String = "ﻩﻪ"
        Dim chars180 As String = "ﻫﻬ"
        Dim chars181 As String = "ﻭﻮو"
        Dim chars182 As String = "ﻯﻰﻱﻲي"
        '        Dim chars184 As String = "ﻳﻴ"
        Dim chars193 As String = "ﺝﺞ"
        Dim chars194 As String = "ﺟﺠ"
        Dim chars195 As String = "ﺡﺢ"
        Dim chars196 As String = "ﺣﺤ"
        Dim chars197 As String = "ﺥﺦ"
        Dim chars198 As String = "ﺧﺨ"
        Dim ret As String = ""
        For i = 0 To unicodestring.Length - 1
            If chars128.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "128" + ret
            ElseIf chars129.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "129" + ret
            ElseIf chars130.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "130" + ret
            ElseIf chars131.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "131" + ret
            ElseIf chars132.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "132" + ret
            ElseIf chars133.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "133" + ret
            ElseIf chars134.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "134" + ret
            ElseIf chars135.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    ret = "135" + ret
                Else
                    ret = "136" + ret
                End If
            ElseIf chars137.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "137" + ret
            ElseIf chars138.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    ret = "138" + ret
                Else
                    ret = "139" + ret
                End If
            ElseIf chars140.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "140" + ret
            ElseIf chars141.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "141" + ret
            ElseIf chars142.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "142" + ret
            ElseIf chars143.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "143" + ret
            ElseIf chars144.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "144" + ret
            ElseIf chars145.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "145" + ret
            ElseIf chars146.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "146" + ret
            ElseIf chars148.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "148" + ret
            ElseIf chars149.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "149" + ret
            ElseIf chars151.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "151" + ret
            ElseIf chars152.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "152" + ret
            ElseIf chars154.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "154" + ret
            ElseIf chars155.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "155" + ret
            ElseIf chars157.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "157" + ret
            ElseIf chars158.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "158" + ret
            ElseIf chars159.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "159" + ret
            ElseIf chars160.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    ret = "160" + ret
                Else
                    ret = "161" + ret
                End If
            ElseIf chars162.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "162" + ret
            ElseIf chars163.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "163" + ret
            ElseIf chars164.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "164" + ret
            ElseIf chars165.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "165" + ret
            ElseIf chars166.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "166" + ret
            ElseIf chars167.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "167" + ret
            ElseIf chars168.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    ret = "168" + ret
                Else
                    ret = "169" + ret
                End If
            ElseIf chars170.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    ret = "170" + ret
                Else
                    ret = "174" + ret
                End If
            ElseIf chars171.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "171" + ret
            ElseIf chars172.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "172" + ret
            ElseIf chars173.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "173" + ret
            ElseIf chars175.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "175" + ret
            ElseIf chars176.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "176" + ret
            ElseIf chars177.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "177" + ret
            ElseIf chars178.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "178" + ret
            ElseIf chars179.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "179" + ret
            ElseIf chars180.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "180" + ret
            ElseIf chars181.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "181" + ret
            ElseIf chars182.Contains(Mid(unicodestring, i + 1, 1)) Then
                If isCut(i + 1, unicodestring.Length, unicodestring) Then
                    ret = "182" + ret
                Else
                    ret = "184" + ret
                End If
            ElseIf chars193.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "193" + ret
            ElseIf chars194.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "194" + ret
            ElseIf chars195.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "195" + ret
            ElseIf chars196.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "196" + ret
            ElseIf chars197.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "197" + ret
            ElseIf chars198.Contains(Mid(unicodestring, i + 1, 1)) Then
                ret = "198" + ret
            Else
                Dim x = AscW(Mid(unicodestring, i + 1, 1)).ToString.PadLeft(3, "0")
                If x.Substring(x.Length - 3, 3).Equals("032") Then
                    ret = x.Substring(x.Length - 3, 3) + ret
                Else
                    ret += x.Substring(x.Length - 3, 3)
                End If
            End If
        Next
        Return ret
    End Function

    Private Shared Function isCut(ByVal pos, ByVal len, ByVal full)
        If pos = len Then
            Return True
        End If
        If Mid(full.ToString, pos + 1, 1).Equals(" ") Then
            Return True
        End If
        Return False
    End Function

    Private Shared Function isConneted(ByVal ch As String)
        If connectedLetters.Contains(ch) Then
            Return True
        End If
        Return False
    End Function


End Class
